const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const reviewSchema = new mongoose.Schema(
	{
		id_user: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			ref: "user",
		},
		id_movie: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			ref: "movie",
		},
		headline: {
			type: String,
		},
		comment: {
			type: String,
			required: true,
		},
		rating: {
			type: Number,
			min: 0,
			max: 10,
			required: true,
		},
	},
	{
		timestamps: {
			createdAt: "createAt",
			updateAt: "updateAt",
		},
	}
);

reviewSchema.index(
	{
		user_id: 1,
		movie_id: 1,
	},
	{
		unique: true,
	}
);

reviewSchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model("review", reviewSchema);
