const validator = require("validator");
const path = require("path");
const { promisify } = require("util");
const crypto = require("crypto");
const mongoose = require("mongoose");
const { actor, movie } = require("../../models/model");

class movieValidator {
  //getValidator
  getValidator = async (req, res, next) => {
    try {
      const errorMessages = [];
      const { page, size, years, rating } = req.query;
      //rating must be number
      //find genres or category

      if ((page && !validator.isInt(page)) || page < 1) {
        errorMessages.push("page must be positive number");
      }

      if ((size && !validator.isInt(size)) || size < 1) {
        errorMessages.push("size must be positive number");
      }

      if (rating && !validator.isInt(rating)) {
        errorMessages.push("rating must be number");
      }

      if (errorMessages.length > 0) {
        return next({ messages: errorMessages, statusCode: 400 });
      }

      next();
    } catch (error) {
      next(error);
    }
  };
  //Get by ID
  getByIdValidator = async (req, res, next) => {
    try {
      const errorMessages = [];

      if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        errorMessages.push("Id is not valid");
      }

      const findMovie = await movie.findOne({ _id: req.params.id });

      if (!findMovie) {
        errorMessages.push("No movie found with this id!");
      }

      if (errorMessages.length > 0) {
        return next({ messages: errorMessages, statusCode: 400 });
      }

      next();
    } catch (error) {
      next(error);
    }
  };
  //masih error
  addValidator = async (req, res, next) => {
    try {
      const errorMessages = [];

      const column = [
        "title",
        "description",
        "synopsis",
        "category",
        "genres",
        "release",
        "director",
        "rating",
      ];

      for (let i = 0; i < column.length; i++) {
        //Check if column is empty
        if (validator.isEmpty(req.body[column[i]])) {
          errorMessages.push(`${column[i]} cannot be empty`);
        }
      }

      //rating must be number
      if (!validator.isInt(req.body.rating)) {
        errorMessages.push("rating must be number");
      }

      //rating must be1 to 10
      if (req.body.rating >= 10 || req.body.rating <= 0) {
        errorMessages.push("rating must be 1 to 10");
      }

      //if name less than 3 char
      if (req.body.description.length <= 3 || req.body.synopsis.length <= 3) {
        errorMessages.push(
          "description and synopsis cannot less than 3 character"
        );
      }

      if (req.body.actor_id.length <= 0) {
        errorMessages.push("actor_id required!");
      }

      // // If image was uploaded
      // if (!req.files) {
      //   errorMessages.push("thumbnail and poster is required");
      // } else {
      //   // req.files.image is come from key (file) in postman
      //   const thumbnail = req.files.thumbnail;
      //   const poster = req.files.poster;

      //   if (!thumbnail) {
      //     errorMessages.push("thumbnail is required");
      //   }

      //   if (!poster) {
      //     errorMessages.push("poster is required");
      //   }

      //   // If error
      //   if (errorMessages.length > 0) {
      //     return next({ statusCode: 400, messages: errorMessages });
      //   }

      //   // Make sure image is photo
      //   if (
      //     !thumbnail.mimetype.startsWith("image") ||
      //     !poster.mimetype.startsWith("image")
      //   ) {
      //     errorMessages.push("File must be an image");
      //   }

      //   // Check file size (max 1MB)
      //   if (thumbnail.size > 1000000 || poster.size > 1000000) {
      //     errorMessages.push("Image must be less than 1MB");
      //   }

      //   // If error
      //   if (errorMessages.length > 0) {
      //     return next({ statusCode: 400, messages: errorMessages });
      //   }

      //   // Create custom filename
      //   let fileName = crypto.randomBytes(16).toString("hex");

      //   // Rename the file
      //   thumbnail.name = `${fileName}${path.parse(thumbnail.name).ext}`;
      //   poster.name = `${fileName}${path.parse(poster.name).ext}`;

      //   // Make file.mv to promise
      //   const moveThumbnail = promisify(thumbnail.mv);
      //   const movePoster = promisify(poster.mv);

      //   // Upload image to /public/images
      //   await moveThumbnail(
      //     `./public/images/movie/thumbnail/${thumbnail.name}`
      //   );
      //   await movePoster(`./public/images/movie/poster/${poster.name}`);

      //   // assign req.body.image with file.name
      //   req.body.thumbnail = thumbnail.name;
      //   req.body.poster = poster.name;
      // }

      //find actor
      const findActor = await actor.findOne({
        _id: req.body.actor_id,
      });

      if (!findActor) {
        errorMessages.push("Actor not found");
      }

      if (errorMessages.length > 0) {
        return next({ messages: errorMessages, statusCode: 400 });
      }

      next();
    } catch (error) {
      next(error);
    }
  };
}

module.exports = new movieValidator();
