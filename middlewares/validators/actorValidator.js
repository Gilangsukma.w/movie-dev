const validator = require("validator");
const path = require("path");
const { promisify } = require("util");
const crypto = require("crypto");
const mongoose = require("mongoose");
const { actor } = require("../../models/model");

class movieValidator {
  //Get by ID
  getByIdValidator = async (req, res, next) => {
    try {
      const errorMessages = [];

      if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        errorMessages.push("Id is not valid");
      }

      const findActor = await actor.findOne({ _id: req.params.id });

      if (!findActor) {
        errorMessages.push("No actor found with this id!");
      }

      if (errorMessages.length > 0) {
        return next({ messages: errorMessages, statusCode: 400 });
      }

      next();
    } catch (error) {
      next(error);
    }
  };
}

module.exports = new movieValidator();
