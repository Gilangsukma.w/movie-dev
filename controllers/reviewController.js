const { user, movie, review } = require("../models/model");

class Reviews {
  async createReview(req, res, next) {
    try {
      const id_movie = req.params;
      const newData = await review.create(req.body);

      const result = await review
        .findOne({ _id: newData._id })
        .populate("id_movie")
        .populate("user.id");

      const data = {
        username: result.id_user.username,
        image: result.id_user.image,
        movie_title: result.id_movie.title,
        rating: result.rating,
        headline: result.headline,
        comment: result.comment,
      };

      const addUser = await user.findOneAndUpdate(id_user, {
        $push: { review: newData._id },
      });
      const addMovie = await movie.findOneAndUpdate(id_movie, {
        $push: { review: newData._id },
      });

      const allReviews = await review.find(id_movie);
      const allRatings = allReview.map((x) => x.rating);
      const average = allRatings.reduce((a, b) => a + b) / allRatings.length;

      const updatedRating = await movie.findOneAndUpdate(id_movie, {
        rating: average,
      });

      res.status(201).json({ message: `Review Created`, data });
    } catch (error) {
      next(error);
    }
  }

  async getAllMoviesReviews(req, res, next) {
    try {
      const id_movies = req.params;

      const allData = await review
        .find(id_movies)
        .populate("id_user", "username")
        .populate("id_movie", "title");
      const findMovie = await movie
        .findOne(id_movie)
        .select("title synopsis thumbnail movieInfo.years");

      for (let i = 0; i < allData.length; i++) {
        allData[i] = {
          username: allData[i].id_user.username,
          image: allData[i].id_user.image,
          movie_title: allData[i].id_movie.title,
          rating: allData[i].rating,
          headline: allData[i].headline,
          comment: allData[i].comment,
        };
      }

      if (allData) {
        const count = req.query.page || 1;
        const page = parseInt(count);
        const limit = 10;
        const startIndex = (page - 1) * limit;
        const endIndex = page * limit;

        const result = {};
        if (endIndex < allData.length) {
          result.next = {
            page: page + 1,
          };
        }
        if (startIndex > 0) {
          result.previous = {
            page: page - 1,
          };
        }

        result.results = allData.slice(startIndex, endIndex);

        res.status(200).json({
          data: {
            page,
            next: result.next,
            movie: findMovie,
            allData,
          },
        });
      }
    } catch (error) {
      next(error);
    }
  }

  async getAllUsersReviews(req, res, next) {
    try {
      const id_user = req.params;
      const allData = await review
        .find(id_user)
        .populate("id_movie", "title")
        .populate("id_user", ("username", "image"));

      for (let i = 0; i < allData.length; i++) {
        allData[i] = {
          username: allData[i].id_user.username,
          image: allData[i].id_user.image,
          movie_title: allData[i].id_movie.title,
          rating: allData[i].rating,
          headline: allData[i].headline,
          comment: allData[i].comment,
        };
      }

      if (allData) {
        const count = req.query.page || 1;
        const page = parseInt(count);
        const limit = 10;
        const startIndex = (page - 1) * limit;
        const endIndex = page * limit;

        const result = {};
        if (endIndex < allReviews.length) {
          result.next = {
            page: page + 1,
          };
        }
        if (startIndex > 0) {
          result.previous = {
            page: page - 1,
          };
        }

        result.results = allReviews.slice(startIndex, endIndex);

        res.status(200).json({
          data: {
            page,
            next: result.next,
            movie: findMovie,
            allData,
          },
        });
      }
    } catch (error) {
      next(error);
    }
  }

  async updateReview(req, res, next) {
    try {
      let updateData = await review.findOneAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true }
      );

      const allReviews = await review.find({ id_movie: updateData.id_movie });
      const allRatings = allReview.map((x) => x.rating);
      const average = allRatings.reduce((a, b) => a + b) / allRatings.length;
      const updatedRating = await movie.findOneAndUpdate(id_movie, {
        rating: average,
      });

      const result = await review
        .findOne(data._id)
        .populate("id_movie")
        .populate("id_user");

      const data = {
        username: result.id_user.username,
        image: result.id_user.image,
        movie_title: result.id_movie.title,
        rating: result.rating,
        headline: result.headline,
        comment: result.comment,
      };

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async deleteReview(req, res, next) {
    try {
      const deletedReview = await review.delete({ _id: req.params.id });

      const allReviews = await review.find({
        id_movie: deletedReview.id_movie,
      });
      const allRatings = allReview.map((x) => x.rating);
      const average = allRatings.reduce((a, b) => a + b) / allRatings.length;
      const updatedRating = await movie.findOneAndUpdate(id_movie, {
        rating: average,
      });

      res.status(200).json({ message: `Review has been deleted` });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Reviews();
