const { movie } = require("../models/model");

class MovieController {
  getController = async (req, res, next) => {
    try {
      const condition = [];

      let { page, size, genres, category, years, rating } = req.query;

      if (genres) {
        condition.push({ genres: genres });
      }

      if (rating) {
        condition.push({ "movieInfo.rating": rating });
      }

      if (category) {
        condition.push({ category: category });
      }
      if (years) {
        years = parseInt(years);
        condition.push({ "movieInfo.years": years });
      }

      if (!page) {
        page = 1;
      }

      if (!size) {
        size = 10;
      }

      const limit = parseInt(size);
      const skip = (page - 1) * size;

      // if condition is 1
      if (condition.length === 1) {
        console.log(condition);
        const data = await movie
          .find(condition[0])
          .select("title category genres thumbnail")
          .limit(limit)
          .skip(skip);

        if (data.length === 0) {
          return next({ message: "No Movie!", statusCode: 404 });
        }

        const total = data.length;
        return res
          .status(200)
          .json({ filter: "1 Condition", page, limit, total, data });
      }

      // if (condition.length >= 1 && condition.length < 5) {
      //   condition.map(async (con) => {
      //     const data = await movie
      //       .find({
      //         $and: condition,
      //       })
      //       .select("title category genres thumbnail")
      //       .limit(limit)
      //       .skip(skip);

      //     if (data.length === 0) {
      //       return next({ message: "No Movie!", statusCode: 404 });
      //     }

      //     const total = data.length;
      //     return res.status(200).json({
      //       filter: `${condition.length} filter`,
      //       page,
      //       limit,
      //       total,
      //       data,
      //     });
      //   });
      // }

      //if condition is 2
      if (condition.length === 2) {
        console.log(condition);
        const data = await movie
          .find({
            $and: [condition[0], condition[1]],
          })
          .select("title category genres thumbnail")
          .limit(limit)
          .skip(skip);

        if (data.length === 0) {
          return next({ message: "No Movie!", statusCode: 404 });
        }

        const total = data.length;
        return res
          .status(200)
          .json({ filter: "2 Contition", page, limit, total, data });
      }

      //if condition is 3
      if (condition.length === 3) {
        console.log(condition);
        const data = await movie
          .find({
            $and: [condition[0], condition[1], condition[2]],
          })
          .select("title category genres thumbnail")
          .limit(limit)
          .skip(skip);

        if (data.length === 0) {
          return next({ message: "No Movie!", statusCode: 404 });
        }

        const total = data.length;
        return res
          .status(200)
          .json({ filter: "3 Contition", page, limit, total, data });
      }

      //if condition is 4
      if (genres && category && years && rating) {
        console.log("all true");
        const data = await movie
          .find({
            "movieInfo.years": years,
            $and: [
              { genres: genres },
              { category: category },
              { "movieInfo.rating": rating },
            ],
          })
          .select("title category genres thumbnail")
          .limit(limit)
          .skip(skip);

        if (data.length === 0) {
          return next({ message: "No Movie!", statusCode: 404 });
        }

        const total = data.length;
        return res
          .status(200)
          .json({ filter: "And filter", page, limit, total, data });
      }

      //No condition
      if (!genres && !category && !years && !rating) {
        console.log("no condition");
        const data = await movie
          .find()
          .select("title category genres thumbnail")
          .limit(limit)
          .skip(skip);

        if (data.length === 0) {
          return next({ message: "No Movie!", statusCode: 404 });
        }

        const total = data.length;
        return res
          .status(200)
          .json({ filter: "No filter", page, limit, total, data });
      }
    } catch (error) {
      next(error);
    }
  };

  getbyIdController = async (req, res, next) => {
    try {
      const data = await movie
        .findOne({
          _id: req.params.id,
        })
        .select("-deleted")
        .populate("actors", "name");

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  };

  addController = async (req, res, next) => {
    try {
      const newData = await movie.create(req.body);

      const data = await movie
        .findOne({
          _id: newData._id,
        })
        .populate("actor");
    } catch (error) {}
  };

  updateController = async (req, res, next) => {
    try {
    } catch (error) {}
  };
  deleteController = async (req, res, next) => {
    try {
    } catch (error) {}
  };
}

module.exports = new MovieController();

// discord presence
