const express = require("express");
const route = express.Router();

//import authorization
// const { admin, userOrAdmin } = require("../middlewares/auth");

//  import validator
const {
  getValidator,
  getByIdValidator,
  addValidator,
} = require("../middlewares/validators/movieValidator");

// import controller
const {
  getController,
  getbyIdController,
  addController,
} = require("../controllers/movieController");

//  routes

//get all with pagination
route.get("/", getValidator, getController);

//get by id
route.get("/:id", getByIdValidator, getbyIdController);

route.post("/", addValidator, addController);

route.put("/:id");
route.delete("/:id");

module.exports = route;
