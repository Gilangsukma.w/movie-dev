const route = require("express").Router();

//import authorization
// const { admin, userOrAdmin } = require("../middlewares/auth");

//  import validator
const {
  getByIdValidator,
} = require("../middlewares/validators/actorValidator");

// import controller
const { getAllActor, getById } = require("../controllers/actorController");

//  routes

//get all with pagination
route.get("/", getAllActor);
route.get("/:id", getByIdValidator, getById);

module.exports = route;
