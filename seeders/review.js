const { user, review, movie } = require("../models/model");
const faker = require("faker");

class reviewSeeder {
  // Seeder add
  addReview = async () => {
    const userList = await user.find();
    const movies = await movie.find();
    for (let i = 0; i < 10; i++) {
      await review.create({
        username: userList[Math.floor(Math.random() * userList.length)]._id,
      });
    }
    console.log("Review have been added");
  };

  // Seeder delete
  removeReview = async () => {
    await review.remove();
    console.log("Review have been deleted");
  };
}

module.exports = new reviewSeeder();
