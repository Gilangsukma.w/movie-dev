const { addCategory, removeCategory } = require("./category");
const { addGenres, removeGenre } = require("./genres");
const { addActor, removeActor } = require("./actor");
const { addReview, removeReview } = require("./review");
const { addMovies, removeMovies } = require("./movies");

// Add
async function add() {
  await Promise.all([addMovies()]);
  // await addActor();
}

// Remove
async function remove() {
  await Promise.all([removeMovies()]);
  // await removeActor();
}

if (process.argv[2] === "add") {
  add()
    .then(() => {
      console.log("Seeders success");
      process.exit(0);
    })
    .catch((err) => console.error(err));
} else if (process.argv[2] === "remove") {
  remove()
    .then(() => {
      console.log("Seeders deleted");
      process.exit(0);
    })
    .catch((err) => console.error(err));
}
