require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
}); // Config environment

const express = require("express"); // Import express
const fileUpload = require("express-fileupload");

const app = express(); // Make express app

/* Import errorHandler */
const errorHandler = require("./middlewares/errorHandler/errorHandler");

/* Import routes */
const auth = require("./routes/authRoute");
const reviews = require("./routes/reviewRoutes");
const category = require("./routes/categoryRoutes");
const genre = require("./routes/genreRoute");
const movie = require("./routes/moviesRoutes");
const actor = require("./routes/actorRoutes");

/* Enables req.body */
app.use(express.json()); // Enables req.body (JSON)
// Enables req.body (url-encoded)
app.use(
  express.urlencoded({
    extended: true,
  })
);

/* Enable req.body and req.files (form-data) */
app.use(fileUpload());

/* Make public folder for static file */
app.use(express.static("public"));

/* Use the routes */
app.use("/api/v1/actor", actor);
app.use("/api/v1/movie", movie);

app.use(express.static("public"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(fileUpload());

app.use("/api/v1/reviews", reviews);
app.use("/api/v1/category", category);
app.use("/api/v1/genre", genre);
app.use("/auth", auth);

/* If route not found */
app.all("*", (req, res, next) => {
  try {
    next({ message: "Endpoint not found", statusCode: 404 });
  } catch (error) {
    next(error);
  }
});

/* Use error handler */
app.use(errorHandler);

if (process.env.NODE_ENV !== "test") {
  app.listen(5000, () => console.log("Listening to 5000"));
}

module.exports = app;
